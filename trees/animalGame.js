
/**
 * @typedef  {Object|null} BinaryNode
 * @property {BinaryNode} leftChild
 * @property {BinaryNode} rightChild
 */

class BinaryNode {
  leftChild = null;
  rightChild = null;
}

class QuestionNode extends BinaryNode {
  constructor(question) {
    super();
    this.value = question;
  }
}

class AnimalNode extends BinaryNode {
  constructor(animal) {
    super();
    this.animal = true;
    this.value = animal;
  }
}

class AnimalGame {
  static YES_ANSWER = 'yes';
  static isConfirmed = answer => answer === AnimalGame.YES_ANSWER;

  firstAttempt = true;
  rootQuestion = new QuestionNode('Is it a mammal?');

  ask = question => globalThis.prompt(question);

  notify = message => globalThis.alert(message);

  askForExtraQuestion = (targetAnimal, guessAnimal) =>
    this.ask(
      `What question could I ask to differentiate between a(n) ${guessAnimal} and a(n) ${targetAnimal}?`,
    );

  buildInitialTree = () => {
    const withScales = new QuestionNode('Does it have scales?');
    withScales.rightChild = new AnimalNode('bird');

    const barks = new QuestionNode('Does it bark?');
    barks.leftChild = new AnimalNode('dog');
    barks.rightChild = new AnimalNode('cat');

    this.rootQuestion.leftChild = barks;
    this.rootQuestion.rightChild = withScales;


    const breathWater = new QuestionNode('Does it breath water?');
    breathWater.leftChild = new AnimalNode('fish');
    breathWater.rightChild = new AnimalNode('snake');
  
    withScales.leftChild = breathWater;
  }

  confirm = targetAnimal =>
    this.ask(`Is the answer to this question true for a(n) ${targetAnimal}?`);

  findOutAnimal = () => this.ask('What is your animal?');

  guessAnimal = animal => this.ask(`Is it a(n) ${animal}?`);

  updateTreeWithNewAnimal = (lastQuestion, animal) => {
    const targetAnimalName = this.findOutAnimal();
    const targetAnimal = new AnimalNode(targetAnimalName);

    let extraQuestion;
    let confirmed = false;

    while (confirmed !== AnimalGame.YES_ANSWER) {
      const extra = this.askForExtraQuestion(targetAnimalName, animal);
      extraQuestion = new QuestionNode(extra);
      confirmed = this.confirm(targetAnimalName);
    }

    // rebuild the tree adding new question and animal
  
    extraQuestion.rightChild = lastQuestion.rightChild;
    lastQuestion.rightChild = extraQuestion;
    extraQuestion.leftChild = targetAnimal;
  }

  traverseOrGuess = (question, child) => {
    if (child && !child.animal) {
      this.traverse(child);
    } else {
      if (AnimalGame.isConfirmed(this.guessAnimal(child.value))) {
        this.notify('I knew it!');
      } else {
        this.updateTreeWithNewAnimal(question, child.value);
      }
    }
  }

  traverse = (question) => {
    if (AnimalGame.isConfirmed(this.ask(question.value))) {
      this.traverseOrGuess(question, question.leftChild);
    } else {
      this.traverseOrGuess(question, question.rightChild);
    }
  }

  // leftChild - yes, rightChild - no
  // leaf nodes are animals, internal nodes are questions
  start = () => {
    if (this.firstAttempt) {
      this.buildInitialTree();

      this.firstAttempt = false;
    }

    this.showIntro();
    this.traverse(this.rootQuestion);
  
    if (AnimalGame.isConfirmed(this.wannaPlayAgain())) {
      this.start();
    }
  };

  showIntro = () => {
    this.notify('Think about some animal and I try to guess it! Answers are "yes" and "no".');
  }

  wannaPlayAgain = () => this.ask('Wanna play again?');
}

/*
  Paste this file contents and next uncommented to the browser console to play:

  const game = new AnimalGame();
  game.start();
*/

export default AnimalGame;
