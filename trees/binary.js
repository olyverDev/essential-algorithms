
/**
 * @typedef  {Object|null} BinaryNode
 * @property {BinaryNode} leftChild
 * @property {BinaryNode} rightChild
 */

class BinaryNode {
  leftChild = null;
  rightChild = null;
}

/* Traversal algorithms */

/**
 * Big O - O(N)
 * - could be rewritten for tree with degree > 2
 * @param   {BinaryNode} binaryNode - to traverse the full tree should be root node
 * @param   {Function} process
 * @returns {undefined}
 */
const traversePreorder = (binaryNode, process) => {
  process(binaryNode);

  if (binaryNode.leftChild !== null) {
    traversePreorder(binaryNode);
  }

  if (binaryNode.rightChild !== null) {
    traversePreorder(inaryNode.rightChild);
  }
}

/**
 * Big O - O(N)
 * - could be rewritten for tree with degree > 2
 * @param   {BinaryNode} binaryNode - to traverse the full tree should be root node
 * @param   {Function} process
 * @returns {undefined}
 */
const traversePostorder = (binaryNode, process) => {
  if (binaryNode.leftChild !== null) {
    traversePreorder(binaryNode);
  }

  if (binaryNode.rightChild !== null) {
    traversePreorder(inaryNode.rightChild);
  }

  process(binaryNode);
}

/**
 * Big O - O(N)
 * - only for binary tree
 * @param   {BinaryNode} binaryNode - to traverse the full tree should be root node
 * @param   {Function} process
 * @returns {undefined}
 */
const traverseInorder = (binaryNode, process) => {
  if (binaryNode.leftChild !== null) {
    traversePreorder(binaryNode);
  }

  process(binaryNode);

  if (binaryNode.rightChild !== null) {
    traversePreorder(binaryNode.rightChild);
  }
}

/**
 * Big O - O(N)
 * @param   {BinaryNode} rootNode
 * @param   {Function} process
 * @returns {undefined}
 */
const traverseDepthFirst = (rootNode, process) => {
  const childrenQueue = [rootNode];

  while (children.length) {
    // it's better to use real queue data structure because 'shift' less perfomant than 'dequeue'
    const nextNode = children.shift();

    process(nextNode);

    if (nextNode.leftChild !== null) {
      childrenQueue.push(nextNode.leftChild);
    }

    if (nextNode.rightChild !== null) {
      childrenQueue.push(nextNode.rightChild);
    }
  }
}

/* BST algorithms, BST - binary sorted tree */

class IntegerBinaryNode extends BinaryNode {
  constructor (value) {
    this.value = value;
  }
}

/**
 * Big O - O(N)
 * check is binary tree sorted
 * @param   {IntegerBinaryNode} rootNode
 * @returns {boolean}
 */
const isBST = (rootNode) => {
  let sorted = true;
  const childrenQueue = [rootNode];

  while (children.length) {
    let { value } = children.shift();

    if (nextNode.leftChild !== null) {
      if (value <= nextNode.leftChild.value) {
        sorted = false;
        break;
      } else {
        childrenQueue.push(nextNode.leftChild);
      }
    }

    if (nextNode.rightChild !== null) {
      if (value > nextNode.rightChild.value) {
        sorted = false;
        break;
      } else {
        childrenQueue.push(nextNode.rightChild);
      }
    }
  }

  return sorted;
}

/* 
  If implement some of these functions as Node class methods
  there will be no need in recursion, but I'll stick to fn approach
*/

/**
 * Big O - O(log N) - O(N^2)
 * @param {number} newValue 
 * @param {IntegerBinaryNode} node 
 */
const addNode = (newValue, node) => {
  if (newValue < node.value) {
    if (node.leftChild === null) {
      node.leftChild = new IntegerBinaryNode(newValue);
    } else {
      addNode(newValue, node.leftChild);
    }
  } else {
    if (node.rightChild === null) {
      node.rightChild = new IntegerBinaryNode(newValue);
    } else {
      addNode(newValue, node.rightChild);
    }
  }
}

// treesort ?

/**
 * Big O - O(log N)
 * @param {number} targetValue 
 * @param {IntegerBinaryNode} node 
 */
const findNode = (targetValue, node) => {
  if (targetValue === node.value) {
    return node;
  }

  if (targetValue < node.value) {
    if (node.leftChild === null) {
      return null;
    }

    return findNode(targetValue, node.leftChild);
  } else {
    if (node.rightChild === null) {
      return null;
    }

    return findNode(targetValue, node.rightChild);
  }
}

// delete node ? (many cases)
