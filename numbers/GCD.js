/**
 * Euclidian algorithm of finding greatest common divisor
 * according to Rod Stephens "Essential algorithms"
 * Big O - O(1)
 * @param {number} a - integer expected
 * @param {number} b - integer expected
 * @returns {number} - greatest common divisor
 */
export const GCD = (a, b) => {
  let num = a;
  let other = b;

  while (other !== 0) {
    const remainder = num % other;
    num = other;
    other = remainder;
  }
  return Math.abs(num);
};

export const GCDRecursive = (a, b) =>
  (b === 0 ? Math.abs(a) : GCDRecursive(b, a % b));

export default GCD;
