
/**
 * @param   {number} input
 * @returns {number[]}
 */
export const decimalToBinaryArray = (input) => {
  let denary = input;

  const bins = [];
  while (denary > 0) {
    bins.push(denary % 2);
    denary = Math.floor(denary / 2);
  }

  return bins.reverse();
};

/**
 * @param   {number} input
 * @returns {number}
 */
export const binaryToDecimal = (input) => {
  let ret = 0;

  let binary = input;
  let base = 1;
  while (binary > 0) {
    ret += (binary % 10) * base;
    binary = Math.floor(binary / 10);
    base = base * 2;
  }

  return ret;
};