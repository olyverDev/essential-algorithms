
/**
 * 
 * @param   {number} min
 * @param   {number} max
 * @returns {number}
 */
const getPseudoRandomIntegerInRange = (min, max) =>
  Math.floor(Math.random() * (max - min) + min);

/*
  - Big O: O(N)
  - Use cases:
      1. Just shuffling (e.g. for working dates)
      2. For quicksort if there is possibility that array was already ordered somehow
      3. Take N random entities from the list (e.g. top 3 winners from 1000 people)
  - Gives equitable order of elements
  - No sense to shuffle many times (repeat the function again, so let's copy the array)
*/

/**
 * 
 * @param   {Array<*>} array
 * @returns {Array<*>} - shuffled
 */
const getRandomizedArray = (array = []) => {
  if (array.length < 2) return array;

  const copy = array.slice();
  const maxIdx = copy.length - 1; // 2

  for (let i = 0; i <= maxIdx; i += 1) {
    const pseudorandomIdx = getPseudoRandomIntegerInRange(i, maxIdx);

    [copy[i], copy[pseudorandomIdx]] = [copy[pseudorandomIdx], copy[i]];
  }

  return copy;
};

export default getRandomizedArray;
