

/**
 * @typedef {Object} Range
 * @param   {number} min
 * @param   {number} max
 */

/**
 * @param   {number[]} array
 * @param   {Range} range 
 * @returns {number[]}
 */
const buildCountsInRange = (array, { min, max }) => {
  const counts = [];

  // making placeholders for counts to get rid checks
  for (let i = min; i <= max; i += 1) {
    counts[i] = 0;
  }

  // collect number of occurences
  array.forEach((el) => {
    counts[el] += 1;
  });

  return counts;
};

/**
 * Big O - O(n + k) k makes sense if range is more than n, otherwise O(n)
 * @param {number[]} array - numbers supposed to be positive
 * @param {Range} range 
 */
const countingSort = (array, range) => {
  const counts = buildCountsInRange(array, range);
  let z = 0;

  // this optimization can replace all the steps below; TODO: understand how it works
  // for (let i = range.min; i <= range.max; i++) {
  //   while (count[i] - 1 > 0) {
  //       arr[z + 1] = i;
  //   }
  // }

  // cumulate values of two neighbors
  for (let i = 1; i < counts.length; i += 1) {
    counts[i] += counts[i - 1];
  }

  // shifting
  counts.unshift(0); // add 0 to head, shift all indexes, why 0 ???
  counts.pop(); // remove last element as it will be out of range

  // sorting
  const sorted = []; // TODO: in place

  for (let i = 0; i < array.length; i += 1) {
    sorted[counts[array[i]]] = array[i]; // hashing
    counts[array[i]] += 1; // incrementing after sort because of the duplicate values possible
  }

  return sorted;
}

export default countingSort;
