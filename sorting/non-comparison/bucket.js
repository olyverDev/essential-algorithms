
// TODO: finish
const bucketSort = (array = [], n) => {
  const buckets = [];

  for (let i = 0; i <= n; i += 1) {
    buckets.push([]);
  }
  // [4,3,2,1,0]
  array.forEach((el) => {
    buckets[n * el] = el;
  });

  buckets.forEach((bucket) => {
    bucketSort(bucket, bucket.length);
  });

  const ret = [];
  buckets.forEach((bucket) => {
    ret.push(...bucket);
  });

  return buckets;
};
