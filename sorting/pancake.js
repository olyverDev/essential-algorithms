
/**
 * @param   {number[]} array
 * @param   {number} size
 * @returns {number}
 */
const findMaxIdx = (array = [], size = array.length) => {
  let max = 0;
  for (let i = 0; i < size; i += 1) {
    if (array[i] > array[max]) {
      max = i;
    }
  }

  return max;
};

/**
 * @param   {number[]} array
 * @param   {number} idx
 * @returns {number[]}
 */
const flipUpToIndex = (array = [], idx = array.length - 1) => {
  let upTo = idx;

  for (let i = 0; i <= upTo; i += 1) {
    [array[upTo], array[i]] = [array[i], array[upTo]];
    upTo -= 1;
  }

  return array;
};

/**
 * Big O - O(N^2)
 * Similar to selection sort, but with help of flips (with find of the max index)
 * @param   {number[]} array
 * @returns {number[]}
 */
const pancakeSort = (array = []) => {
  let currentSize = array.length;

  while (currentSize > 1) {
    flipUpToIndex(array, findMaxIdx(array, currentSize));
    flipUpToIndex(array, currentSize - 1);
    currentSize -= 1;
  }

  return array;
};

export default pancakeSort;
