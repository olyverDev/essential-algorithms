/*
  Steps (recursive is classic):
  1. If data length is less than or equal to 1, return data, no need to sort more
  Otherwise choose the pivot element (the last element in our case)
  2. Pick out all elements less than pivot (left part)
  3. Pick out all elements greated than pivot (right part)
  4. Repeat 1-3 incrementally for `less than pivot` elements
  5. Repeat 1-3 incrementally for `greater than pivot` elements
  6. For each iteration join results in the next way: left part, pivot element, right part
*/


/**
 * Big O - O(n^2)
 * @param   {number[]} array
 * @returns {number[]}
 */
const quickSort = (array = []) => {
  if (array.length <= 1) return array;

  const pivot = array[array.length - 1];
  const left = array.slice(array.length).filter(el => el <= pivot);
  const right = array.filter(el => el > pivot);

  return [...quickSort(left), pivot, ...quickSort(right)];
};

export default quickSort;
