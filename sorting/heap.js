
// TOOD: describe steps for myself

/**
 * to represent binary heap as array
 * @param   {number} parentIdx
 * @returns {number}
 */
const getLeftChildIdx = (parentIdx) => 2 * parentIdx + 1;
const getRightChildIdx = (parentIdx) => 2 * parentIdx + 2;

/**
 * @param   {number[]} array
 * @param   {number} max
 * @param   {number} index
 * @returns {number[]}
 */
const heapify = (array, max, index) => {
  let largest = index;
  const leftIdx = getLeftChildIdx(largest);
  const rightIdx = getRightChildIdx(largest);

  if (leftIdx < max && array[largest] < array[leftIdx]) {
    largest = leftIdx;
  }

  if (rightIdx < max && array[largest] < array[rightIdx]) {
    largest = rightIdx;
  }

  if (largest !== index) {
    [array[index], array[largest]] = [array[largest], array[index]];
    heapify(array, max, largest);
  }

  return array;
};

/**
 * @param   {number[]} array
 * @returns {number[]}
 */
const buildMaxHeap = (array) => {
  // because child indices are found by next formula: 2 * {parent index} + 1 / 2 (left / right)
  // no sense to check other elements
  let lastIdx = Math.floor(array.length / 2) - 1;
  while (lastIdx >= 0) {
    heapify(array, array.length, lastIdx);
    lastIdx -= 1;
  }

  return array;
}

/**
 * @param   {number[]} array
 * @returns {number[]}
 */
const heapSort = (array = []) => {
  buildMaxHeap(array);

  for (let lastIdx = array.length - 1; lastIdx > 0; lastIdx -= 1) {
    // Move current root to end 
    [array[0], array[lastIdx]] = [array[lastIdx], array[0]];

    // call max heapify on the reduced heap 
    heapify(array, lastIdx, 0);
  }

  return array;
};

export default heapSort;
