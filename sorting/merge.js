/*
  Steps (recursive is classic):
  1. If data length is less than or equal to 1, return data, no need to sort more
  Otherwise find middle index
  2. Split data into 2 halves by index (left and right)
  3. Take two halves, compare first element of left to first element of the right
  If left has lower value, move it from left to new result data supposed to be sorted
  Othwerwise move the first element of right to new result data supposed to be sorted
  4. Build new data in the next order: result data, left, right
  5. Repeat 1-4 recursively until you reach condition described in 1
*/

/**
 * 
 * @param   {number[]} left
 * @param   {number[]} right
 * @returns {number[]}
 */
const merge = (left, right) => {
  const result = [];
  
  while (left.length && right.length) {
    if (left[0] < right[0]) {
      result.push(left.shift());
    } else {
      result.push(right.shift());
    }
  }

  return [...result, ...left, ...right];
}

/**
 * Big O - O(n * log(n))
 * @param   {number[]} array
 * @returns {number[]}
 */
const mergeSortRecursive = (array = []) => {
  if (array.length <= 1) {
    return array;
  }

  const middleIdx = Math.floor(array.length / 2);
  const left = array.splice(0, middleIdx);

  return merge(
    mergeSortRecursive(left),
    mergeSortRecursive(array),
  );
}

// TODO: iterative approach

export default mergeSortRecursive;
