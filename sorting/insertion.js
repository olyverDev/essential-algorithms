
/*
  Steps (assuming ascending):
  1. Select second element (with index = 1)
  2. Compare selected element to all previous elements
  3.
    If all previous elements are less than current, stay as is
    Else find the very left element which is greater, move current element to position before it
  5. Repeat from 1 to 3 incrementally until array end
*/

/**
 * Big O - O(N^2)
 * @param   {number[]} array
 * @returns {number[]}
 */
const insertionSort = (array = []) => {
  for (let i = 1; i < array.length; i += 1) {
    let currentEl = array[i];

    let prevIdx = i - 1;
    while (prevIdx >= 0 && currentEl < array[prevIdx]) {
      array[prevIdx + 1] = array[prevIdx];
      prevIdx -= 1;
    }

    array[prevIdx + 1] = currentEl;      
  }

  return array;
};

// TODO: recursive

export default insertionSort;
