
/*
  Steps:
  1. Find minimum or maximum value (asc or desc)
  2. Delete it from current data and insert it to the head of the new sorted data
  3. Repeat 1 and 2 for current data until it became empty
*/

/**
 * Big O - O(N^2), as findIndex used maybe a bit more like O(2n^2)
 * @param {number[]} array
 * @param {'asc'|'desc'} dir
 * @returns {number[]}
 */
export const selectionSort = (array = [], dir = 'asc') => {
  const sorted = [];
  const remaining = [...array];

  let baseEl;
  const baseFinder = dir === 'desc' ? Math.max : Math.min;

  while (sorted.length !== array.length) {
    // finding min or max value
    baseEl = baseFinder(...remaining);

    // pushing found value to sorted array
    sorted.push(baseEl);

    // deleting found value from left subarray
    // TODO: it's better to get rid of findIndex, but find index with custom fn, not with Math.max or min
    remaining.splice(remaining.findIndex(el => el === baseEl), 1);
  }

  return sorted;
};

/**
 * Big O - O(N^2), as findIndex used maybe a bit more like O(2n^2)
 * @param {number[]} array
 * @param {'asc'|'desc'} dir
 * @returns {number[]}
 */
export const selectionSortInPlace = (array = [], dir = 'asc') => {
  let baseEl;
  let baseElIdx;
  const baseFinder = dir === 'desc' ? Math.max : Math.min;

  const left = [...array];
  let currentSwapIdx = 0;

  while (left.length) {
    // finding min or max value
    baseEl = baseFinder(...left);

    // deleting found value from left subarray
    const findPredicate = el => el === baseEl;
    left.splice(left.findIndex(findPredicate), 1);

    // swapping found element with next not sorted
    baseElIdx = array.findIndex(findPredicate);
    [array[currentSwapIdx], array[baseElIdx]] = [array[baseElIdx], array[currentSwapIdx]];
    currentSwapIdx += 1;
  }

  return array;
};

export default selectionSortInPlace;
