
/*
  Steps:
  1. Select first element
  2. Compare it to next element
  3. Swap if needed
  4. Repeat incrementally 1-3 until there will be no swaps (one loop without swaps is needed to determine it)
  (Another way of thinking about it - last elements will be in place decrementally)
*/

/**
 * Big O - O(N^2)
 * @param   {number[]} array
 * @returns {number[]}
 */
export const bubbleSort = (array = []) => {
  const count = array.length;

  for (let i = 0; i < count; i += 1) {
    for (let j = 0; j < count - i; j += 1) { // lasts are already sorted
      if (array[j] > array[j + 1]) {
        // swap elements if current greater than next
        [array[j], array[j + 1]] = [array[j + 1], array[j]];
      } 
    }
  }

  return array;
};

/**
 * Big O - O(N^2)
 * @param   {number[]} array
 * @param   {number} count
 * @returns {number[]}
 */
export const bubbleSortRecursive = (
  array = [],
  count = array.length,
) => {
  if (count <= 1) return array; // base case

  for (let i = 0; i < count; i += 1) {
    if (array[i] > array[i + 1]) {
      // swap elements if current greater than next
      [array[i], array[i + 1]] = [array[i + 1], array[i]];
    } 
  }

  // last element is already sorted
  return bubbleSortRecursive(array, count - 1);
};

/**
 * my implementation before reading smarter solution
 * Big O - O(N^2)
 * @param   {number[]} array
 * @returns {number[]}
 */
const bubbleSortDummy = (array = []) => {
  let withoutAnySwaps = false;
  const count = array.length;

  while (!withoutAnySwaps) {
    let swaps = false;

    for (let i = 0; i < count; i += 1) {
      const last = i === count - 1;

      if (!last && array[i] > array[i + 1]) {
        // swap elements if it is not last and current greater than next
        [array[i], array[i + 1]] = [array[i + 1], array[i]];
        swaps = true;
      } 

      if (last && !swaps) {
        // if it's last and there were not any swaps array is sorted (see while condition)
        withoutAnySwaps = true;
      }
    }
  }

  return array;
};

export default bubbleSort;
