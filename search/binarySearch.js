
/**
 * Big O - O(log N)
 * @param {number} value
 * @param {number[]} sortedIntArray
 * @returns {number|undefined} - index
 */
const binarySearch = (value, sortedIntArray = []) => {
  let min = 0;
  let max = sortedIntArray.length - 1;
  let mid;

  while (min <= max) {
    mid = Math.round((max + min) / 2);
    if (value < sortedIntArray[mid]) {
      max = mid - 1;
    } else if (value > sortedIntArray[mid]) {
      min = mid + 1;
    } else {
      return mid;
    }
  }
}

export default binarySearch;
