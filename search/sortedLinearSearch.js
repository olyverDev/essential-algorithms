
/**
 * Big O - O(N)
 * @param {number} value
 * @param {number[]} sortedIntArray
 * @returns {number|undefined} - index
 */
const sortedLinearSearch = (value, sortedIntArray = []) => {
  const count = sortedIntArray.length;

  for (let i = 0; i < count; i += 1) {
    if (sortedIntArray[i] === value) {
      return i;
    }

    if (sortedIntArray[i] > value) {
      return undefined;
    }
  }
}

export default sortedLinearSearch;
