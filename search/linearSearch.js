
/**
 * Big O - O(N)
 * @param {number} value
 * @param {number[]} intArray
 * @returns {number|undefined} - index
 */
const linearSearch = (value, intArray = []) => {
  const count = intArray.length;

  for (let i = 0; i < count; i += 1) {
    if (value === intArray[i]) {
      return i;
    }
  }
}

export default linearSearch;
