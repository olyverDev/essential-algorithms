
const isParenthesesProperlyNested = (expression = '') => {
  if (!expression.length) throw new Error('Empty string');

  const characters = expression.split('');

  let counter = 0;

  for (let i = 0; i < characters.length; i += 1) {
    if (characters[i] === '(') {
      counter += 1;
    } else if (characters[i] === ')') {
      counter -= 1;

      if (counter < 0) return false;
    }
  }

  return counter === 0;
}

export default isParenthesesProperlyNested;
